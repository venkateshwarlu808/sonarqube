{{ if .Release.IsInstall }}
apiVersion: batch/v1
kind: Job
metadata:
  name: "set-password"
spec:
  template:
    metadata:
      labels:
        job: set-password
    spec:
      restartPolicy: OnFailure
      initContainers:
        - name: await-startup
          image: "docker.io/curlimages/curl:8.4.0@sha256:4a3396ae573c44932d06ba33f8696db4429c419da87cbdc82965ee96a37dd0af"
          command:
            - sh
            - -c
            - until timeout 10 curl --fail http://sonarqube/; do echo "not yet up"; sleep 5; done
      containers:
        - name: set-password
          image: docker.io/library/postgres:16.1-alpine@sha256:bebdbe026fbb097684cc853e1a259b8c8b385e06ffc9873b4f76c5ae5d596257
          command:
            - bash
            - -c
            - until PGPASSWORD=sonar psql -d sonar -U sonar -h postgres -c
              "update users set crypted_password='100000\$eAP4z5e9o6OgGX4nY41QI7LrmA1mK6UnIua4xrYjeBfxW2n0voXegzKqhCfGmC92AP3PBwUGo5DbRbKVFuWPNA==', salt='WMbxTJSfVaDFYvF1kXxG2R6nCzE=', reset_password='false', hash_method='PBKDF2' where login='admin';";
              do echo "setting password failed"; sleep 5; done; echo "password is set"
{{ end }}
